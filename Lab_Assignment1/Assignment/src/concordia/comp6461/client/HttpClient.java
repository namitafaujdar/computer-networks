package concordia.comp6461.client;

import java.io.*;
import java.io.BufferedReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * HttpClient class is the main class for the HttpClient implementation
 *
 * @author NagaSatishReddy
 * @author Namita Faujdar
 */

public class HttpClient {
    String command;
    List<String> commandParsedList;
    String commandUrl;
    UrlData urlData;
    private boolean verbose;
    private boolean header;
    private ArrayList<String> headerDataList = new ArrayList<String>();
    private String inlineData;
    private boolean inLineFlag;

    public static void main(String[] args) {
        boolean toBeRunning = true;
        HttpClient client = new HttpClient();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            while(toBeRunning) {
                String input = reader.readLine();
                if (input.equalsIgnoreCase("exit")) {
                    toBeRunning = false;
                } else {
                    client.resetQueryParameters();
                    client.setCommand(input);
                    client.setCommandParseList();
                    client.parseCommand();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                reader.close();
            } catch (IOException e) {
                System.out.println("Problem in closing the buffer reader");
            }
        }
    }

    private void resetQueryParameters() {
        this.setVerbose(false);
        this.setInLineFlagData(false);
        this.setHeader(false, "");
    }

    private void parseCommand() {
        if(!this.commandParsedList.get(0).equals(Constants.HTTPC)){
            printInvalidCommand();
            return;
        }
        if(this.commandParsedList.get(1).equals(Constants.HELP)){
            parseHelp();
        }else if(this.commandParsedList.get(1).equals(Constants.GET) || this.commandParsedList.get(1).equals(Constants.POST)){
            int i = 2;
            while(i < this.commandParsedList.size()){
                String currentWord = this.commandParsedList.get(i);
                if(currentWord.startsWith(Constants.HTTP) || currentWord.startsWith(Constants.HTTPS)){
                    setCommandUrl(currentWord);
                }else{
                    switch(currentWord){
                        case Constants.VERBOSE:
                            this.setVerbose(true);
                            break;
                        case Constants.HEADER:
                            i++;
                            this.setHeader(true, this.commandParsedList.get(i));
                            break;
                        case Constants.INLINE_DATA_1:
                        case Constants.INLINE_DATA_2:
                            this.setInLineFlagData(true);
                            this.inlineData = this.commandParsedList.get(++i)+" "+this.commandParsedList.get(++i);
                            break;
                    }
                }
                i++;
            }
            executeCommand(this.commandParsedList.get(1));
        }else{
            printInvalidCommand();
            return;
        }
    }

    private void setInLineFlagData(boolean b) {
        this.inLineFlag = b;
    }

    private void setHeader(boolean b, String s) {
        this.header=b;
        if(b){
            if(this.headerDataList == null){
                this.headerDataList = new ArrayList<String>();
            }
            this.headerDataList.add(s);
        }else{
            this.headerDataList = new ArrayList<String>();
        }
    }

    private void setVerbose(boolean b) {
        this.verbose = b;
    }

    private void executeCommand(String requestMethod) {
        this.urlData = new UrlData(this.commandUrl).fetchUrlData();
        try{
            if(requestMethod.equals(Constants.GET)){
                executeGetRequest();
            }else if(requestMethod.equals(Constants.POST)){
                executePostRequest();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executePostRequest() throws IOException {
        Socket socket = null;
        try {
            socket = new Socket(this.urlData.getHostName(), this.urlData.getPortNumber());
            StringBuilder stringBuilder = new StringBuilder();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            writer.write(generateOutputBasedOnMethodType("POST"));
            writer.write("Host:"+this.urlData.getHostName()+"\r\n");
            if(this.header && !headerDataList.isEmpty()) {
                for(String headerData : headerDataList) {
                    String[] headerKeyValue = headerData.split(":");
                    writer.write(headerKeyValue[0] + ":" + headerKeyValue[1] + "\r\n");
                }
            }

            if(this.inLineFlag){
                stringBuilder.append(this.inlineData);
            }
            writer.write("Content-Length:" + stringBuilder.toString().trim().length() + "\r\n");
            writer.write("\r\n");
            if (this.inlineData != null) {
                writer.write(this.inlineData.replace("\'", "")+"\r\n");
            }
            if (stringBuilder.toString().trim().length() >= 1) {
                writer.write(stringBuilder.toString() + "\r\n");
            }
            writer.flush();
            printOutput(socket);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            socket.close();
        }
    }

    private void executeGetRequest() throws IOException {
        Socket socket = null;
        PrintWriter writer = null;
        try {
            socket = new Socket(this.urlData.getHostName(), this.urlData.getPortNumber());
            writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(generateOutputBasedOnMethodType("GET"));
            writer.println("Host:"+this.urlData.getHostName());
            for(int i = 0; i < this.headerDataList.size(); i++){
                String[] headerKeyValue = this.headerDataList.get(i).split(":");
                writer.println(headerKeyValue[0]+":"+headerKeyValue[1]);
            }
            writer.println("\r\n");
            printOutput(socket);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            writer.close();
            socket.close();
        }
    }

    private void printOutput(Socket socket) throws IOException {
        BufferedReader bufferedReader = null;
        InputStream inputStream = null;
        try {
            inputStream = socket.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            boolean isFirstTime = true;
            StringBuilder receiveContent = new StringBuilder();
            do{
                line = bufferedReader.readLine();
                if(line.trim().isEmpty() && isFirstTime){
                    receiveContent.append("\r");
                    isFirstTime = false;
                }
                receiveContent.append(line);
                receiveContent.append("\n");
            }while(!(line.endsWith("}")) || line.endsWith("</html>"));
            String [] receivedContent = receiveContent.toString().split("\r");
            String [] responseHeader = receivedContent[0].split("\n");
            String [] responseBody = receivedContent[1].split("\n");

            printVerbose(responseHeader);
            printTheResponseBody(responseBody);
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            bufferedReader.close();
            inputStream.close();
        }

    }

    private void printVerbose(String[] responseHeader) {
        if(this.verbose){
            printTheResponseBody(responseHeader);
        }
    }

    private void printTheResponseBody(String[] responseBody) {
        for(String data: responseBody){
            System.out.println(data);
        }
    }

    private String generateOutputBasedOnMethodType(String requestMethod) {
        boolean urlPathIsEmpty = this.urlData.getUrlPath().isEmpty();
        StringBuilder stringBuilder = new StringBuilder();
        String endHttp = requestMethod.equals("POST") ? "HTTP/1.1\r\n": urlPathIsEmpty ? "/1.1" : "HTTP/1.1";
        stringBuilder.append(requestMethod).append(" ").append(urlPathIsEmpty ? "/": this.urlData.getUrlPath()).append(" ").append(endHttp);
        return stringBuilder.toString();
    }

    private void setCommandUrl(String currentWord) {
        this.commandUrl = currentWord;
    }

    private void printInvalidCommand() {
        System.out.println("Invalid Command");
    }

    private void parseHelp() {
        if(this.commandParsedList.size() >3){
            printInvalidCommand();
            return;
        }
        String lastWord = this.commandParsedList.get(this.commandParsedList.size()-1);
        if(lastWord.equals(Constants.GET)){
            System.out.println("httpc help get\n" +
                    "usage: httpc get [-v] [-h key:value] URL\n" +
                    "Get executes a HTTP GET request for a given URL.\n" +
                    " -v Prints the detail of the response such as protocol, status,\n" +
                    "and headers.\n" +
                    " -h key:value Associates headers to HTTP Request with the format\n" +
                    "'key:value'.");
        }else if(lastWord.equals(Constants.POST)){
            System.out.println("httpc help post\n" +
                    "Comp 6461 – Fall 2020 - Lab Assignment # 1 Page 7\n" +
                    "usage: httpc post [-v] [-h key:value] [-d inline-data] [-f file] URL\n" +
                    "Post executes a HTTP POST request for a given URL with inline data or from\n" +
                    "file.\n" +
                    " -v Prints the detail of the response such as protocol, status,\n" +
                    "and headers.\n" +
                    " -h key:value Associates headers to HTTP Request with the format\n" +
                    "'key:value'.\n" +
                    " -d string Associates an inline data to the body HTTP POST request.\n" +
                    " -f file Associates the content of a file to the body HTTP POST\n" +
                    "request.\n" +
                    "Either [-d] or [-f] can be used but not both.");
        }else{
            System.out.println("httpc help\n" +
                    "httpc is a curl-like application but supports HTTP protocol only.\n" +
                    "Usage:\n" +
                    " httpc command [arguments]\n" +
                    "The commands are:\n" +
                    " get executes a HTTP GET request and prints the response.\n" +
                    " post executes a HTTP POST request and prints the response.\n" +
                    " help prints this screen.\n" +
                    "Use \"httpc help [command]\" for more information about a command.");
        }
    }

    private void setCommandParseList() {
        this.commandParsedList = Stream.of(this.command.trim().split(" ")).filter(item->item.length() > 0).map(String::trim).collect(Collectors.toList());
    }

    private void setCommand(String input) {
        this.command = input;
    }
}
